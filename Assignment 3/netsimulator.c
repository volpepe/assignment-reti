#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/types.h>
#include <netinet/in.h>

/* ******************************************************************
   This code should be used for unidirectional data transfer protocols
   (from A to B)
   Network properties:
   - one way network delay averages five time units (longer if there
     are other messages in the channel for Pipelined ARQ), but can be larger
   - packets can be corrupted (either the header or the data portion)
     or lost, according to user-defined probabilities
   - packets will be delivered in the order in which they were sent
     (although some can be lost).
 **********************************************************************/

/* a "msg" is the data unit passed from layer 5 (teachers code) to layer  */
/* 4 (students' code).  It contains the data (characters) to be delivered */
/* to layer 5 via the students transport level protocol entities.         */
struct msg {
    char data[20];
};

/* a packet is the data unit passed from layer 4 (students code) to layer */
/* 3 (teachers code).  Note the pre-defined packet structure, which all   */
/* students must follow. */
struct pkt {
    int seqnum;
    int acknum;
    int checksum;
    char payload[20];
};

//acknum = next expected seqnum from other host

/*- Your Definitions
   ---------------------------------------------------------------------------*/
/** MY STRUCTS **/

struct sent_pkt {
    struct pkt packet; //the packet that has been sent
    double lifetime; //when this packet is going to generate a timeout
    struct sent_pkt* next; //pointer to next sent packet.
};

struct buffered_pkt {
    struct pkt packet; //the packet that has been buffered
    struct buffered_pkt* next; //a pointer to the next packet
};

/** MY FUNCTIONS **/

void put_in_buffer(struct buffered_pkt**, struct pkt);
struct pkt get_from_buffer(struct buffered_pkt**);
void resend_from_sent_pkts(int);
void put_in_sent_pkts(struct pkt);
void remove_from_sent_pkts(int);
void look_for_new_base();
int in(struct buffered_pkt*, struct pkt);

void print_in_sent_pkts();

int acceptable(int, int*);
void update_acceptables(int, int*);
void update_lifetimes();
int calculate_checksum(struct pkt);
int check_checksum(struct pkt);

void print_received_buffer();
void print_received_buffer();

int acceptable(int, int*);
void update_acceptables(int, int*);
int calculate_checksum(struct pkt);
int check_checksum(struct pkt);
void timercheck();
void print_stats();
void deal_sending_packets(struct pkt);
struct pkt makepkt(struct msg);


/** STATISTICS **/

int transmitted_packets = 0;
int retransmissions = 0;
int ack_packets = 0;
int corrupted_packets = 0;

/** BUFFERS **/

#define MAX_BUFFER_SIZE 50

struct buffered_pkt *sending_queue, *receiving_buffer;
struct sent_pkt *sent_pkts;
int sent;
int in_queue;

int *B_acceptable_packets, *A_acceptable_acks, *A_sendable_packets;

/** VARIABLES **/

/** A **/
int A_base, A_nextseqnum;

/** B **/
int B_base;

/** GENERAL **/
double last_timer_start;

/*----------------------------------------------------------------------------*/

/* Please use the following values in your program */

#define   A    0
#define   B    1
#define   FIRST_SEQNO   0

/*- Declarations ------------------------------------------------------------*/
void  restart_rxmt_timer(void);
void  tolayer3(int AorB, struct pkt packet);
void  tolayer5(char datasent[20]);

void  starttimer(int AorB, double increment);
void  stoptimer(int AorB);

/* WINDOW_SIZE, RXMT_TIMEOUT and TRACE are inputs to the program;
   We have set an appropriate value for LIMIT_SEQNO.
   You do not have to concern but you have to use these variables in your
   routines --------------------------------------------------------------*/

extern int WINDOW_SIZE;      // size of the window
extern int LIMIT_SEQNO;      // when sequence number reaches this value, it wraps around
extern double RXMT_TIMEOUT;  // retransmission timeout
extern int TRACE;            // trace level, for your debug purpose
extern double time_now;      // simulation time, for your debug purpose

/********* YOU MAY ADD SOME ROUTINES HERE ********/

/******************************************************************/
/** LINKED LISTS-RELATED CHECKING/UPDATING FUNCTIONS **/
/******************************************************************/

void put_in_buffer(struct buffered_pkt** buffer, struct pkt packet) {
    struct buffered_pkt* head = *buffer;
    struct buffered_pkt* prev = NULL;
    struct buffered_pkt* current = head;

    struct buffered_pkt* to_insert = (struct buffered_pkt*) malloc(sizeof(struct buffered_pkt));
    to_insert->packet = packet;

    while (current != NULL) {
        /*if (packet.seqnum == (current->packet).seqnum) {
            printf("Packet %d is already in queue! Ignored.\n", packet.seqnum);
            return;
        }*/
        prev = current;
        current = current->next;
    }

    if (prev == NULL) {
        /* CASE 1: HEAD OF THE BUFFER */
        if (head == NULL) {
                /* CASE 1.1: EMPTY BUFFER */
                to_insert->next = NULL;
        } else {
                /* CASE 1.2: FIRST PLACE */
                to_insert->next = head;
        }
        *buffer = to_insert;
    } else {
        /* CASE 2: IN THE MIDDLE OF THE BUFFER */
        to_insert->next = prev->next;
        prev->next = to_insert;
    }
}

struct pkt get_from_buffer(struct buffered_pkt** head)
{
    struct pkt packet;
    //takes the first buffered_pkt from the list
    struct buffered_pkt* elem = *head;

    *head = elem->next;
    packet = elem->packet;
    free(elem);
    printf("A: Getting the following packet from buffer: seq %d, ack %d\n", packet.seqnum, packet.acknum);
    return(packet);
}

void resend_from_sent_pkts(int to_find)
{
    struct sent_pkt* current = sent_pkts;
    struct pkt to_send;
    int found = 0;

    while (current != NULL) {
        if ((current->packet).seqnum == to_find) {
            found = 1;
            to_send = current->packet;
            break;
        } else {
            current = current->next;
        }
    }

    if (found) {
        remove_from_sent_pkts(to_send.seqnum);
        printf("A: Sending the following packet: seq %d, ack %d\n", to_send.seqnum, to_send.acknum);
        tolayer3(A, to_send);
        put_in_sent_pkts(to_send);
        print_in_sent_pkts();
    }
}

void put_in_sent_pkts(struct pkt packet)
{
    struct sent_pkt* current = sent_pkts;
    struct sent_pkt* prev = NULL;

    struct sent_pkt* to_insert = (struct sent_pkt*) malloc(sizeof(struct sent_pkt));
    to_insert->packet = packet;
    to_insert->next = NULL;

    /*lifetime will be set relatively to the currently running timer*/
    to_insert->lifetime = RXMT_TIMEOUT + (time_now - last_timer_start);

    if (current != NULL) {
        while(current->lifetime <= to_insert->lifetime) {
            //ordered by lifetime and by arriving order in case lifetime is the same
            prev = current;
            current = current->next;
            if (current == NULL) {
                break;
            }
        }
    }

    if (prev == NULL) {
        /* CASE 1: HEAD OF THE BUFFER */
        if (sent_pkts != NULL) {
            /* CASE 1.1: FIRST PLACE */
            to_insert->next = sent_pkts;
        }
        sent_pkts = to_insert;
    } else {
        /* CASE 2: IN THE MIDDLE OF THE BUFFER */
        to_insert->next = prev->next;
        prev->next = to_insert;
    }
}

void remove_from_sent_pkts(int acknum)
{
    struct sent_pkt* current_sent = sent_pkts, *prev = NULL;
    int removed = 0;

    while (current_sent != NULL && !removed) {
        if ((current_sent->packet).seqnum == acknum) {
            if (prev == NULL) {
                sent_pkts = current_sent->next;
            } else {
                prev->next = current_sent->next;
            }
            removed = 1;
        } else {
            prev = current_sent;
            current_sent = current_sent->next;
        }
    }

    if (removed) {
        printf("Removing packet with seqnum %d from list\n", acknum);
        free(current_sent);
        print_in_sent_pkts();
    }
}

void look_for_new_base()
{
    //look_for_new_base starts from current B base and looks whether the next seqno is in the receiving buffer.
    //if it is in the buffer, current base gets incremented and that packet gets sent to B's layer5.
    struct buffered_pkt* current = receiving_buffer, *prev = NULL;
    int found = 0;

    do {
        //immediately updates base
        B_base = ( B_base + 1 ) % LIMIT_SEQNO;
        found = 0;
        //look for following packet in buffer: if none next section is basically ignored.
        while (current != NULL) {
            if ((current->packet).seqnum == (B_base + 1) % LIMIT_SEQNO) {
                found = 1;
                break;
            } else {
                prev = current;
                current = current->next;
            }
        }
        if (found) {
            printf("B: Found following packet! Sending seq %d, ack %d to application too\n", (current->packet).seqnum, (current->packet).acknum);
            //remove from buffer, send to layer 5, put current back to receiving buffer
            printf("The content of this packet is the following: %s\n", (current->packet).payload);
            if (prev == NULL) {
                receiving_buffer = current->next;
            } else {
                prev->next = current->next;
            }
            tolayer5((current->packet).payload);
            free(current);
            //reset
            print_received_buffer();
            current = receiving_buffer;
            prev = NULL;
        }
    } while (found);
}

int in(struct buffered_pkt* buf, struct pkt packet)
{
    struct buffered_pkt* current = buf;

    while(current != NULL) {
        if ((current->packet).seqnum == packet.seqnum) {
            return 1;
        }
        current = current->next;
    }

    return 0;
}

/************* DEBUG ***************/

void print_in_sent_pkts()
{
    struct sent_pkt* current = sent_pkts;

    printf("In sent_packets:\t");
    while (current != NULL) {
        printf("%d\t", (current->packet).seqnum);
        current = current->next;
    }
    printf("\n");
}

void print_received_buffer()
{
    struct buffered_pkt* current = receiving_buffer;

    printf("In receiving buffer:\t");
    while (current != NULL) {
        printf("%d\t", (current->packet).seqnum);
        current = current->next;
    }
    printf("\n");
}

/******************************************************************/
/** GENERAL CHECKING/UPDATING FUNCTIONS **/
/******************************************************************/

int acceptable(int seqnum, int* array)
{
    for (int i = 0; i < WINDOW_SIZE; i++) {
        if (array[i] == seqnum) {
            return 1;
        }
    }
    return 0;
}

void update_acceptables(int base, int* array)
{
    int start_from = base;

    for (int i = 0; i < WINDOW_SIZE; i++) {
        array[i] = start_from;
        start_from = ( start_from + 1 ) % LIMIT_SEQNO;
    }
}

void update_lifetimes()
{
    struct sent_pkt* current = sent_pkts;

    printf("A: Updating lifetimes...\n");
    while(current != NULL) {
        current -> lifetime -= (time_now - last_timer_start);
        printf("A: Packet %d has now a lifetime of %lf\n", (current -> packet).seqnum, current -> lifetime);
        current = current -> next;
    }
}

int calculate_checksum(struct pkt pack) {
    int checksum = 0;
    checksum += (pack.seqnum + pack.acknum);

    for (int l = 0; l < 20; l++) {
        checksum += (int) pack.payload[l];
    }
    return ~checksum;
}

int check_checksum(struct pkt pack) {
    //will return 1 if pkt has no problems, 0 otherwise.
    int complement = calculate_checksum(pack);
    if (~complement + pack.checksum == -1) {
        return 1;
    }
    return 0;
}

void timercheck() 
{
    //stopping timer
    stoptimer(A);
    update_lifetimes();

    if (sent > 0) {
        //if there are other packets in the sent list, start a timer for the first one.
        starttimer(A, sent_pkts->lifetime);
        printf("A: Started a timer of %lf for %d\n", sent_pkts->lifetime, (sent_pkts->packet).seqnum);
        last_timer_start = time_now;
    }
}

void print_stats()
{
    printf("Total sent packets: %d\n", transmitted_packets);
    printf("Number of retransmissions: %d\n", retransmissions);
    printf("Number of ACKs: %d\n", ack_packets);
    printf("Number of corrupted packets: %d\n", corrupted_packets);
    printf("Number of packets left in queue: %d\n", in_queue);
}

/** PUSHING/CREATING PACKETS **/

void deal_sending_packets(struct pkt packet)
{
    printf("A: Sending the following packet: seq %d, ack %d\n", packet.seqnum, packet.acknum);
    tolayer3(A, packet);
    if (A_base == A_nextseqnum) {
        printf("A: Starting timer...\n");
        starttimer(A, RXMT_TIMEOUT);
        last_timer_start = time_now;
    }
    put_in_sent_pkts(packet);
    print_in_sent_pkts();
}

struct pkt makepkt(struct msg message) {
    struct pkt packet;

    packet.seqnum = A_nextseqnum;
    packet.acknum = A_nextseqnum;
    for (int l = 0; l < 20; l++) {
        packet.payload[l] = message.data[l];
    }
    packet.checksum = calculate_checksum(packet);

    return packet;
}

/********* STUDENTS WRITE THE NEXT SIX ROUTINES *********/

/* called from layer 5, passed the data to be sent to other side */
void A_output (struct msg message)
{
    int in_buffer = 0;
    struct pkt packet = makepkt(message);

    //if the packet we are sending can be contained in window, send it asap
    if (acceptable(A_nextseqnum, A_sendable_packets) && sent < WINDOW_SIZE) {
        deal_sending_packets(packet);
        sent++;
        transmitted_packets++;
    } else {
        in_queue++;
        if (in_queue < MAX_BUFFER_SIZE) {
            //otherwise if buffer is not full store it in there
            put_in_buffer(&sending_queue, packet);
            printf("A: Inserting the following packet in queue: seq %d, ack %d\n", packet.seqnum, packet.acknum);
            printf("A: There are currently %d packets in queue.\n", in_queue);
            in_buffer = 1;
        } else {
            printf("A: Sending buffer overflow!! Aborting...\n");
            exit(1);
        }
    }

    //updating nextseqnum as this ALWAYS happens.
    A_nextseqnum = (A_nextseqnum + 1) % LIMIT_SEQNO;
    if (!in_buffer) {
       //if the packet was saved into the buffer, there's no need to update the acceptables array.
       update_acceptables(A_nextseqnum, A_sendable_packets);
       //acceptables will be updated when window moves.
    }

}

/* called from layer 3, when a packet arrives for layer 4 */
void A_input(struct pkt packet)
{
    printf("A: ACK received: seq %d, ack %d\n", packet.seqnum, packet.acknum);
    if (check_checksum(packet)) {
        //packet was not corrupt
        if (acceptable(packet.acknum, A_acceptable_acks)) {
            //updating base
            int old_base = A_base;
            A_base = (packet.acknum + 1) % LIMIT_SEQNO;

            if (old_base > A_base) {
                //base wrapped around
                for (int i = old_base; i < LIMIT_SEQNO; i++) {
                    remove_from_sent_pkts(i);
                    sent--;
                }
                old_base = 0;
            }

            for (int i = old_base; i < A_base; i++) {
                //removing ACKed packet(s) from sent packet list
                remove_from_sent_pkts(i);
                sent--;
            }

            timercheck();

            //checking sending buffer in case there are packets waiting to be sent
            while (sent < WINDOW_SIZE) {
                if (in_queue > 0) {
                    struct pkt to_send = get_from_buffer(&sending_queue);
                    in_queue--;
                    printf("A: Removed a packet from queue. There are currently %d packets in queue.\n", in_queue);
                    deal_sending_packets(to_send);
                    sent++;
                    transmitted_packets++;
                } else {
                    //if there are no packets in buffer, well, just wait for new packets to arrive
                    printf("No packets in buffer.\n");
                    break;
                }
            }
        } else {
            /*if we arrive here the ACKnum is basically lower than A's acceptable range.
            Since B ACKs his base (the last packet he's received) and A changes his base adding 1 to the ACKnum, we can be sure
            that A's base is B's base + 1: therefore the packet we need to send back to B is just the one with seqnum "acknum + 1".*/
            printf("A: ACK was repeated! Resending packet %d\n", (packet.acknum + 1) % LIMIT_SEQNO);
            resend_from_sent_pkts((packet.acknum + 1) % LIMIT_SEQNO);
            timercheck();
            retransmissions++;
        }
    } else {
        printf("A: ACK was corrupt and will be ignored\n");
        corrupted_packets++;
    }
    //eventually updating acceptables. This could result in overwriting numbers with the same numbers.
    update_acceptables(A_base, A_acceptable_acks);
}

/* called when A's timer goes off */
void A_timerinterrupt (void)
{
    /* Update lifetimes and start a timer. The timeout can only happen on the first packet of the buffer.*/
    /* If a timer is running, THEORETICALLY there is at least one packet in sent_pkts, so no need to check for NULL pointers */
    struct pkt to_send = sent_pkts->packet;

    printf("A: Timer interrupt! Resending first sent packet... (%d)\n", to_send.seqnum);
    remove_from_sent_pkts(to_send.seqnum);
    update_lifetimes();
    last_timer_start = time_now;
    printf("A: Re-adding %d to sent_pkts\n", to_send.seqnum);
    put_in_sent_pkts(to_send);
    starttimer(A, sent_pkts->lifetime);
    tolayer3(A, to_send);
    printf("A: Started a timer of %lf for %d\n", sent_pkts->lifetime, (sent_pkts->packet).seqnum);
    retransmissions++;
}

/* the following routine will be called once (only) before any other */
/* entity A routines are called. You can use it to do any initialization */
void A_init (void)
{
    sent = 0;
    in_queue = 0;
    A_base = 1;
    A_nextseqnum = 1;

    sent_pkts = NULL;
    sending_queue = NULL;

    A_acceptable_acks = (int*)malloc(sizeof(int)*WINDOW_SIZE);
    A_sendable_packets = (int*)malloc(sizeof(int)*WINDOW_SIZE);

    for (int i = 1; i < WINDOW_SIZE; i++) {
        A_acceptable_acks[i] = i;
        A_sendable_packets[i] = i;
    }

    fflush(stdin);
    atexit(print_stats);
}

/* called from layer 3, when a packet arrives for layer 4 at B*/
void B_input (struct pkt packet)
{
    if (check_checksum(packet)) {
        printf("B: Healthy packet arrived! seq %d, ack %d while base is %d\n", packet.seqnum, packet.acknum, B_base);
        if (acceptable(packet.seqnum, B_acceptable_packets)) {
            if (packet.seqnum != (B_base + 1) % LIMIT_SEQNO) {
                //we must put the packet into the receiving buffer only if we don't have it already.
                if (!in(receiving_buffer, packet)) {
                    printf("B: Inserting following packet in buffer: seq %d, ack %d\n", packet.seqnum, packet.acknum);
                    put_in_buffer(&receiving_buffer, packet);
                } else {
                    printf("B: We already have this packet, but thanks anyway - I guess\n");
                }
            } else {
              printf("B: Sending seq %d, ack %d to application\n", packet.seqnum, packet.acknum);
              printf("The content of this packet is the following: %s\n", packet.payload);
              tolayer5(packet.payload);
              look_for_new_base();
              printf("B: New base is %d\n", B_base);
              update_acceptables((B_base + 1) % LIMIT_SEQNO, B_acceptable_packets);
            }
        }
        //ALWAYS ack the base
        print_received_buffer();
        struct pkt ack;
        ack.acknum = B_base;
        ack.seqnum = B_base;
        ack.checksum = calculate_checksum(ack);
        printf("B: Sending ack: seq %d, ack %d\n", ack.seqnum, ack.acknum);
        tolayer3(B, ack);
        ack_packets++;
    } else {
        printf("B: Packet seq %d, ack %d was corrupt and will be ignored.\n", packet.seqnum, packet.acknum);
        corrupted_packets++;
    }
}

/* the following routine will be called once (only) before any other */
/* entity B routines are called. You can use it to do any initialization */
void B_init (void)
{
    B_base = 0;
    B_acceptable_packets = (int*)malloc(sizeof(int)*WINDOW_SIZE);
    for (int i = 1; i < WINDOW_SIZE; i++) {
        B_acceptable_packets[i] = i;
    }
}

/*****************************************************************
 ***************** NETWORK EMULATION CODE STARTS BELOW ***********
   The code below emulates the layer 3 and below network environment:
   - emulates the tranmission and delivery (possibly with bit-level corruption
    and packet loss) of packets across the layer 3/4 interface
   - handles the starting/stopping of a timer, and generates timer
    interrupts (resulting in calling students timer handler).
   - generates message to be sent (passed from layer 5 to 4)

   THERE IS NOT REASON THAT ANY STUDENT SHOULD HAVE TO READ OR UNDERSTAND
   THE CODE BELOW.  YOU SHOLD NOT TOUCH, OR REFERENCE (in your code) ANY
   OF THE DATA STRUCTURES BELOW.  If you're interested in how I designed
   the emulator, you're welcome to look at the code - but again, you shouldn't have
   to, and you definitely should not have to modify
 ******************************************************************/


struct event {
        double evtime;     /* event time */
        int evtype;       /* event type code */
        int eventity;     /* entity where event occurs */
        struct pkt *pktptr; /* ptr to packet (if any) assoc w/ this event */
        struct event *prev;
        struct event *next;
};
struct event *evlist = NULL;   /* the event list */

/* Advance declarations. */
void init(void);
void generate_next_arrival(void);
void insertevent(struct event *p);


/* possible events: */
#define  TIMER_INTERRUPT 0
#define  FROM_LAYER5     1
#define  FROM_LAYER3     2

#define  OFF             0
#define  ON              1


int TRACE = 0;              /* for debugging purpose */
int fileoutput;
double time_now = 0.000;
int WINDOW_SIZE;
int LIMIT_SEQNO;
double RXMT_TIMEOUT;
double lossprob;            /* probability that a packet is dropped  */
double corruptprob;         /* probability that one bit is packet is flipped */
double lambda;              /* arrival rate of messages from layer 5 */
int ntolayer3;             /* number sent into layer 3 */
int nlost;                 /* number lost in media */
int ncorrupt;              /* number corrupted by media*/
int nsim = 0;
int nsimmax = 0;
unsigned int seed[5];         /* seed used in the pseudo-random generator */

int
main(int argc, char **argv)
{
    struct event *eventptr;
    struct msg msg2give;
    struct pkt pkt2give;

    int i,j;

    init();
    A_init();
    B_init();

    while (1) {
        eventptr = evlist; /* get next event to simulate */
        if (eventptr==NULL)
            goto terminate;
        evlist = evlist->next; /* remove this event from event list */
        if (evlist!=NULL)
            evlist->prev=NULL;
        if (TRACE>=2) {
            printf("\nEVENT time: %f,",eventptr->evtime);
            printf("  type: %d",eventptr->evtype);
            if (eventptr->evtype==0)
                printf(", timerinterrupt  ");
            else if (eventptr->evtype==1)
                printf(", fromlayer5 ");
            else
                printf(", fromlayer3 ");
            printf(" entity: %d\n",eventptr->eventity);
        }
        time_now = eventptr->evtime; /* update time to next event time */
        if (eventptr->evtype == FROM_LAYER5 ) {
            generate_next_arrival(); /* set up future arrival */
            /* fill in msg to give with string of same letter */
            j = nsim % 26;
            for (i=0; i<20; i++)
                msg2give.data[i]=97+j;
            msg2give.data[19]='\n';
            nsim++;
            if (nsim==nsimmax+1)
                break;
            A_output(msg2give);
        } else if (eventptr->evtype ==  FROM_LAYER3) {
            pkt2give.seqnum = eventptr->pktptr->seqnum;
            pkt2give.acknum = eventptr->pktptr->acknum;
            pkt2give.checksum = eventptr->pktptr->checksum;
            for (i=0; i<20; i++)
                pkt2give.payload[i]=eventptr->pktptr->payload[i];
            if (eventptr->eventity == A) /* deliver packet by calling */
                A_input(pkt2give); /* appropriate entity */
            else
                B_input(pkt2give);
            free(eventptr->pktptr); /* free the memory for packet */
        } else if (eventptr->evtype ==  TIMER_INTERRUPT) {
            A_timerinterrupt();
        } else  {
            printf("INTERNAL PANIC: unknown event type \n");
        }
        free(eventptr);
    }
terminate:
    printf("Simulator terminated at time %.12f\n",time_now);
    return (0);
}


void
init(void)                         /* initialize the simulator */
{
    int i=0;
    printf("----- * ARQ Network Simulator Version 1.1 * ------ \n\n");
    printf("Enter number of messages to simulate: ");
    scanf("%d",&nsimmax);
    printf("Enter packet loss probability [enter 0.0 for no loss]:");
    scanf("%lf",&lossprob);
    printf("Enter packet corruption probability [0.0 for no corruption]:");
    scanf("%lf",&corruptprob);
    printf("Enter average time between messages from sender's layer5 [ > 0.0]:");
    scanf("%lf",&lambda);
    printf("Enter window size [>0]:");
    scanf("%d",&WINDOW_SIZE);
    LIMIT_SEQNO = 2*WINDOW_SIZE;
    printf("Enter retransmission timeout [> 0.0]:");
    scanf("%lf",&RXMT_TIMEOUT);
    printf("Enter trace level:");
    scanf("%d",&TRACE);
    printf("Enter random seed: [>0]:");
    scanf("%d",&seed[0]);
    for (i=1; i<5; i++)
        seed[i]=seed[0]+i;
    fileoutput = open("OutputFile", O_CREAT|O_WRONLY|O_TRUNC,0644);
    if (fileoutput<0)
        exit(1);
    ntolayer3 = 0;
    nlost = 0;
    ncorrupt = 0;
    time_now=0.0;          /* initialize time to 0.0 */
    generate_next_arrival(); /* initialize event list */
}

/****************************************************************************/
/* mrand(): return a double in range [0,1].  The routine below is used to */
/* isolate all random number generation in one location.  We assume that the*/
/* system-supplied rand() function return an int in the range [0,mmm]        */
/****************************************************************************/
int nextrand(int i)
{
    seed[i] = seed[i]*1103515245+12345;
    return (unsigned int)(seed[i]/65536)%32768;
}

double mrand(int i)
{
    double mmm = 32767;
    double x;             /* individual students may need to change mmm */
    x = nextrand(i)/mmm;      /* x should be uniform in [0,1] */
    if (TRACE==0)
        printf("%.16f\n",x);
    return(x);
}


/********************* EVENT HANDLINE ROUTINES *******/
/*  The next set of routines handle the event list   */
/*****************************************************/
void
generate_next_arrival(void)
{
    double x,log(),ceil();
    struct event *evptr;

    if (TRACE>2)
        printf("          GENERATE NEXT ARRIVAL: creating new arrival\n");

    x = lambda*mrand(0)*2; /* x is uniform on [0,2*lambda] */
    /* having mean of lambda        */
    evptr = (struct event *)malloc(sizeof(struct event));
    evptr->evtime =  time_now + x;
    evptr->evtype =  FROM_LAYER5;
    evptr->eventity = A;
    insertevent(evptr);
}

void
insertevent(p)
struct event *p;
{
    struct event *q,*qold;

    if (TRACE>2) {
        printf("            INSERTEVENT: time is %f\n",time_now);
        printf("            INSERTEVENT: future time will be %f\n",p->evtime);
    }
    q = evlist; /* q points to header of list in which p struct inserted */
    if (q==NULL) { /* list is empty */
        evlist=p;
        p->next=NULL;
        p->prev=NULL;
    } else {
        for (qold = q; q !=NULL && p->evtime > q->evtime; q=q->next)
            qold=q;
        if (q==NULL) { /* end of list */
            qold->next = p;
            p->prev = qold;
            p->next = NULL;
        } else if (q==evlist) { /* front of list */
            p->next=evlist;
            p->prev=NULL;
            p->next->prev=p;
            evlist = p;
        } else { /* middle of list */
            p->next=q;
            p->prev=q->prev;
            q->prev->next=p;
            q->prev=p;
        }
    }
}

void
printevlist(void)
{
    struct event *q;
    printf("--------------\nEvent List Follows:\n");
    for(q = evlist; q!=NULL; q=q->next) {
            printf("Event time: %f, type: %d entity: %d\n",q->evtime,q->evtype,q->eventity);
    }
    printf("--------------\n");
}



/********************** Student-callable ROUTINES ***********************/

/* called by students routine to cancel a previously-started timer */
void
stoptimer(AorB)
int AorB;  /* A or B is trying to stop timer */
{
    struct event *q /* ,*qold */;
    if (TRACE>2)
        printf("          STOP TIMER: stopping timer at %f\n",time_now);
    for (q=evlist; q!=NULL; q = q->next)
        if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) {
            /* remove this event */
            if (q->next==NULL && q->prev==NULL)
                evlist=NULL; /* remove first and only event on list */
            else if (q->next==NULL) /* end of list - there is one in front */
                q->prev->next = NULL;
            else if (q==evlist) { /* front of list - there must be event after */
                q->next->prev=NULL;
                evlist = q->next;
            } else { /* middle of list */
                q->next->prev = q->prev;
                q->prev->next =  q->next;
            }   
            free(q);
            return;
        }
    printf("Warning: unable to cancel your timer. It wasn't running.\n");
}


void
starttimer(AorB,increment)
int AorB;  /* A or B is trying to stop timer */
double increment;
{
        struct event *q;
        struct event *evptr;

        if (TRACE>2)
                printf("          START TIMER: starting timer at %f\n",time_now);
        /* be nice: check to see if timer is already started, if so, then  warn */
        /* for (q=evlist; q!=NULL && q->next!=NULL; q = q->next)  */
        for (q=evlist; q!=NULL; q = q->next)
                if ( (q->evtype==TIMER_INTERRUPT  && q->eventity==AorB) ) {
                        printf("Warning: attempt to start a timer that is already started\n");
                        return;
                }

        /* create future event for when timer goes off */
        evptr = (struct event *)malloc(sizeof(struct event));
        evptr->evtime =  time_now + increment;
        evptr->evtype =  TIMER_INTERRUPT;
        evptr->eventity = AorB;
        insertevent(evptr);
}


/************************** TOLAYER3 ***************/
void
tolayer3(AorB,packet)
int AorB;  /* A or B is trying to stop timer */
struct pkt packet;
{
        struct pkt *mypktptr;
        struct event *evptr,*q;
        double lastime, x;
        int i;


        ntolayer3++;

        /* simulate losses: */
        if (mrand(1) < lossprob)  {
                nlost++;
                if (TRACE>0)
                        printf("          TOLAYER3: packet being lost\n");
                return;
        }

        /* make a copy of the packet student just gave me since he/she may decide */
        /* to do something with the packet after we return back to him/her */
        mypktptr = (struct pkt *)malloc(sizeof(struct pkt));
        mypktptr->seqnum = packet.seqnum;
        mypktptr->acknum = packet.acknum;
        mypktptr->checksum = packet.checksum;
        for (i=0; i<20; i++)
                mypktptr->payload[i]=packet.payload[i];
        if (TRACE>2)  {
                printf("          TOLAYER3: seq: %d, ack %d, check: %d ", mypktptr->seqnum,
                       mypktptr->acknum,  mypktptr->checksum);
        }

        /* create future event for arrival of packet at the other side */
        evptr = (struct event *)malloc(sizeof(struct event));
        evptr->evtype =  FROM_LAYER3;/* packet will pop out from layer3 */
        evptr->eventity = (AorB+1) % 2; /* event occurs at other entity */
        evptr->pktptr = mypktptr; /* save ptr to my copy of packet */
        /* finally, compute the arrival time of packet at the other end.
           medium can not reorder, so make sure packet arrives between 1 and 10
           time units after the latest arrival time of packets
           currently in the medium on their way to the destination */
        lastime = time_now;
        for (q=evlist; q!=NULL; q = q->next)
                if ( (q->evtype==FROM_LAYER3  && q->eventity==evptr->eventity) )
                        lastime = q->evtime;
        evptr->evtime =  lastime + 1 + 9*mrand(2);



        /* simulate corruption: */
        if (mrand(3) < corruptprob)  {
                ncorrupt++;
                if ( (x = mrand(4)) < 0.75)
                        mypktptr->payload[0]='?'; /* corrupt payload */
                else if (x < 0.875)
                        mypktptr->seqnum = 999999;
                else
                        mypktptr->acknum = 999999;
                if (TRACE>0)
                        printf("          TOLAYER3: packet being corrupted\n");
        }

        if (TRACE>2)
                printf("          TOLAYER3: scheduling arrival on other side\n");
        insertevent(evptr);
}

void
tolayer5(datasent)
char datasent[20];
{
        write(fileoutput,datasent,20);
}
