alphabet = 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
linenum = 0
length = len(alphabet)
result = True

with open("OutputFile", "r") as f:
	for line in f:
		if (line.startswith(alphabet[linenum % length])):
			linenum += 1
		else:
			result = False
			break

if result == False:
	print("NOT VALID")
else:
	print("VALID")
	print('number of packets correctly arrived: {0}'.format(linenum))