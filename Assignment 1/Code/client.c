/*this is the client application*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <stdint.h>	/* uint64_t */
#include <sys/poll.h>

#include "mynetlib.h"

#define LEGAL_ARGUMENTS 3
#define MAX_BUF_SIZE 1024 	/*UDP messages won't be longer than 1024 bytes*/
#define STDIN 0  // file descriptor for standard input

int main(int argc, char const *argv[]) {

	/******************* VARIABLES *************************/

	/*Addresses of server and client*/
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	
	/*The client is supposed to receive the server address to connect to and its port.*/
	char *server_IP_addr_string = argv[1];	//IP where server is located
	char *server_port_string = argv[2];	//Server port as a string
	int server_port = getInt(server_port_string); //The actual server port as an integer

	/*sfd will be the socket file descriptor of the client*/
	int sfd, stop = 0, rv;

	/*Some lengths for different comparisons*/
	ssize_t byteRec, byteSent;
	size_t msgLen;
	socklen_t serv_size;

	/*Data buffers*/
	char receivedData[MAX_BUF_SIZE];
	char toSendData[MAX_BUF_SIZE];

	/*Array of pollfd structures for the poll() function*/
	struct pollfd ufds[2];

	/********************* INITIALIZATION ***************************/

	/*The client needs to check how many arguments have been passed*/
	if (argc != LEGAL_ARGUMENTS) {
		perror("missing or too many arguments");
		exit(EXIT_FAILURE);
	}
	
	/*Server socket file descriptor*/
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sfd < 0) {
		perror("socket creation failure"); /* Print error message*/
		exit(EXIT_FAILURE);
	}

	/*Structures initialization*/
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(server_port); /*htons converts to network byte order*/
	inet_pton(AF_INET, server_IP_addr_string, &(server_addr.sin_addr));	
	/*inet_pton used instead of inet_addr, which is obsolete. 
	It does the same thing: it converts the server IP string to an address and puts it into
	the sin_addr field of the server structure. */
	serv_size = sizeof(server_addr);

	ufds[0].fd = STDIN;
	ufds[0].events = POLLIN;

	ufds[1].fd = sfd;
	ufds[1].events = POLLIN;

	/*Sets the socket file descriptor to non-blocking so that if errors occur in the poll function, 
	the program will not remain blocked
	fcntl(sfd, F_SETFL, O_NONBLOCK); */

	/********************** ACTION ********************************/

	while(!stop) {
		/*Checks whether there is some input to send to the server or
		the server sent some input to the client's socket*/
		rv = poll(ufds, 2, -1);

		if (rv == -1) {
			perror("Error in poll function");
		} else if (rv > 0) {
			if (ufds[0].revents & POLLIN) {
				/*There is output in stdin ready to be sent out to the server*/
				fgets(toSendData, MAX_BUF_SIZE, stdin); 
				fflush(stdin);

				msgLen = countStrLen(toSendData);
				/*This function sends the "toSendData" string to the server listening on
				socket sfd*/
				byteSent = sendto(sfd, toSendData, msgLen, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
				if (byteSent == -1) {
					perror("sendto");
					exit(EXIT_FAILURE);
				}

				/*Client needs to check if the message is "exit": in this case we wait for an answer and then 
				the application stops.*/
				if(strcmp(toSendData, "exit\n") == 0) {
					stop = 1;
				}
			} if (ufds[1].revents & POLLIN || stop) {
				/*The server has responded and something has been written on the socket:
				therefore we need to obtain and display it.*/
				cleanString(receivedData, MAX_BUF_SIZE);
				/*Wait for bytes to be received. Bytes will be stored into the receivedData string
				and byteRec will contain the number of received bytes*/
				byteRec = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct sockaddr *) &server_addr, &serv_size);
				if (byteRec == -1) {
					perror("recvfrom");
					exit(EXIT_FAILURE);
				}
				/*setting up server's address-related strings*/
				inet_ntop(AF_INET, &(server_addr.sin_addr), server_IP_addr_string, INET_ADDRSTRLEN);

				/*prints received data*/
				//removing newline character for better formatting
				removeNewline(receivedData);
				printf("Message received by %s from port %hu: ", server_IP_addr_string, ntohs(server_addr.sin_port));
				printData(receivedData, byteRec);
				printf(" (%d byte\\s)\n", byteRec);
			}
		}
		fflush(stdin);
	}
}