#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

size_t countStrLen(char *str) {
	size_t c = 0;
    while(*str != '\0'){
        c += 1;
        str++;
    }
    return c;
}

void printData(char* str, size_t numBytes) {
	int i = 0;
	for (; i<numBytes; i++) {
		printf("%c", str[i]);
	}
}

void convertToUpperCase(char *str, size_t numBytes) {
	int i = 0;
    for(; i < numBytes; i++){
        str[i] = toupper(str[i]);
    }
}

int getInt(char *str) {
    return atoi(str);
}

void removeNewline(char *str) {
	int i = 0;
	while(str[i] != '\n' && str[i] != '\0') {
		i++;
	}
	str[i] = '\0';
}

void cleanString(char* str, int MAX_BUF_SIZE) {
	int i = 0;
	for (i = 0; i<MAX_BUF_SIZE; i++) {
		str[i] = '\0';
	}
}
