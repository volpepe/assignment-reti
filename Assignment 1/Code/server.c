/*this is the server application*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <stdint.h>	/* uint64_t */
#include <sys/poll.h>

#include "mynetlib.h"

#define LEGAL_ARGUMENTS 3
#define MAX_BUF_SIZE 1024 	/*UDP messages won't be longer than 1024 bytes*/
#define STDIN 0

int main(int argc, char const *argv[]) {

	/******************* VARIABLES *************************/

	/*Addresses of server and client*/
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	
	/*The server is supposed to receive the its address and port.*/
	char *server_IP_addr_string = argv[1];	//IP where server is located
	char *server_port_string = argv[2];	//Server port as a string
	int server_port = getInt(server_port_string); //The actual server port as an integer

	/*sfd will be the socket file descriptor of the server*/
	int sfd, rv;

	/*Some lengths for different comparisons*/
	ssize_t byteRec, byteSent;
	size_t msgLen;
	socklen_t cli_size;

	/*Data buffers*/
	char receivedData[MAX_BUF_SIZE];
	char toSendData[MAX_BUF_SIZE];

	/*In addition, server needs to print the IP of the client that's contacting it*/
	char client_ip_address_string[INET_ADDRSTRLEN];

	/*Array of pollfd structures for the poll() function*/
	struct pollfd ufds[2];

	/********************* INITIALIZATION ***************************/

	/*The client needs to check how many arguments have been passed*/
	if (argc != LEGAL_ARGUMENTS) {
		perror("missing or too many arguments");
		exit(EXIT_FAILURE);
	}
	
	/*Server creates a socket with file descriptor sfd*/
	sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sfd < 0) {
		perror("socket creation failure"); /* Print error message*/
		exit(EXIT_FAILURE);
	}

	/*Structures initialization*/
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(server_port); /*htons converts to network byte order*/
	inet_pton(AF_INET, server_IP_addr_string, &(server_addr.sin_addr));	
	/*inet_pton used instead of inet_addr, which is obsolete. 
	It does the same thing: it converts the server IP string to an address and puts it into
	the sin_addr field of the server structure. */
	cli_size = sizeof(server_addr);

	/*Sets the socket file descriptor to non-blocking so that if errors occur in the select function, 
	the program will not remain blocked
	fcntl(sfd, F_SETFL, O_NONBLOCK); */

	/*The server application needs to BIND itself to the desired port.
	sfd will contain the socket file descriptor*/
	rv = bind(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
	if (rv < 0) {
		/*check for errors in the bind operation*/
		perror("bind failure");
		exit(EXIT_FAILURE);
	} 

	ufds[0].fd = STDIN;
	ufds[0].events = POLLIN;

	ufds[1].fd = sfd;
	ufds[1].events = POLLIN;

	/********************** ACTION ********************************/

	while(1) {
		/*Checks whether there is some input to send to the server or
		the server sent some input to the client's socket*/
		rv = poll(ufds, 2, -1);

		if (rv == -1) {
			perror("Error in poll function");
		} else if (rv > 0) {
			if (ufds[0].revents & POLLIN) {
				/*There is output in stdin ready to be sent out to the client*/
				fgets(toSendData, MAX_BUF_SIZE, stdin); 
				fflush(stdin);

				msgLen = countStrLen(toSendData);
				/*This function sends the "toSendData" string to the client listening on
				socket sfd*/
				byteSent = sendto(sfd, toSendData, msgLen, 0, (struct sockaddr *) &client_addr, sizeof(client_addr));
				if (byteSent == -1) {
					perror("sendto");
					exit(EXIT_FAILURE);
				}

				/*The server does not send exit messages*/
			} else if (ufds[1].revents & POLLIN) {
				/*The client has written something on the socket:
				therefore we need to obtain and display it.*/
				cleanString(receivedData, MAX_BUF_SIZE);
				/*Wait for bytes to be received. Bytes will be stored into the receivedData string
				and byteRec will contain the number of received bytes*/
				byteRec = recvfrom(sfd, receivedData, MAX_BUF_SIZE, 0, (struct sockaddr *) &client_addr, &cli_size);
				if (byteRec == -1) {
					perror("recvfrom");
					exit(EXIT_FAILURE);
				}
				/*Setting up client's address-related strings*/
				inet_ntop(AF_INET, &(client_addr.sin_addr), client_ip_address_string, INET_ADDRSTRLEN);

				/*prints received data*/
				//removing newline character for better formatting
				removeNewline(receivedData);
				printf("Message received by %s from port %hu: ", client_ip_address_string, ntohs(client_addr.sin_port));
				printData(receivedData, byteRec);
				printf(" (%d byte\\s)\n", byteRec);

				/*If server receives an exit, it prints "goodbye" into a buffer and sends it back to the client.
				Remember the program removes the newline at the end of the string, so "exit" will now be just
				"exit" instead of "exit\n" */
				if (strcmp(receivedData, "exit") == 0) {
					byteSent = sendto(sfd, "Goodbye!\n", strlen("Goodbye!\n"), 0, (struct sockaddr *) &client_addr, sizeof(client_addr));
					if (byteSent == -1) {
						perror("sendto");
						exit(EXIT_FAILURE);
					}
				}
			}
		}
		fflush(stdin);
	}
}